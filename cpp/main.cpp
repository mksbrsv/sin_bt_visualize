#include <boost/asio.hpp>
#include <iostream>
#include <sciplot/sciplot.hpp>

int main() {
  boost::asio::io_context io;
  boost::asio::serial_port port(io);

  std::array<uint8_t, 4> data;
  std::vector<float> vec;
  std::vector<float> xa;
  std::vector<float> ya;

  port.open("/dev/rfcomm0");
  port.set_option(boost::asio::serial_port_base::baud_rate(9600));

  float x;
  float tmp_x;

  while (true) {
    port.read_some(boost::asio::buffer(data, 4));
    memcpy(&x, data.data(), 4);
    port.read_some(boost::asio::buffer(data, 4));
    memcpy(&tmp_x, data.data(), 4);
    if (x == 0 && tmp_x == 0) {
      vec.push_back(x);
      vec.push_back(tmp_x);
      for (int i = 0; i < 100; i++) {
        port.read_some(boost::asio::buffer(data, 4));
        memcpy(&x, data.data(), 4);
        vec.push_back(x);
      }
    }
    if (vec.size() == 102) break;
  }
  for (int i = 0; i < 102; i++) {
    if (i % 2 == 0)
      xa.push_back(vec[i]);
    else
      ya.push_back(vec[i]);
  }
  sciplot::Plot plot;
  plot.legend().hide();
  plot.drawCurve(xa, ya);
  plot.show();
}
