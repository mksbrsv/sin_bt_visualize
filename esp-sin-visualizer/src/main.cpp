#include <Arduino.h>
#include <BluetoothSerial.h>
#include <utility>
#include <vector>
#include <cmath>
#include <bitset>
#include <cstring>

// bt error handling
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

std::vector<float> get_sin(std::vector<float>& dots) {
  std::vector<float> vec;
  for (auto& dot : dots) {
    vec.push_back(std::sin(dot));
  }
  return vec;
}

std::vector<float> make_vector(float begin, float end, float step) {
  std::vector<float> vec;
  vec.reserve((end-begin) / step + 1);
  while (begin <= end) {
    vec.push_back(begin);
    begin += step;
  }
  return vec;
}

void float_to_bytes(byte arr[], float x) {
  std::memcpy(arr, &x, 4);
}

void bt_callback(esp_spp_cb_event_t event, esp_spp_cb_param_t* param) {
  if (event == ESP_SPP_SRV_OPEN_EVT) {
    Serial.println("Client connected");
    digitalWrite(LED_BUILTIN, HIGH);
  }

  if (event == ESP_SPP_CLOSE_EVT) {
    Serial.println("Client disconnected");
    digitalWrite(LED_BUILTIN, LOW);
  }
}

BluetoothSerial SerialBT;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  SerialBT.begin("esp32");
  Serial.println("Device started, pair it");
}

std::vector<float> x = make_vector(0, 5, 0.1);
std::vector<float> y = get_sin(x);

void loop() {
  SerialBT.register_callback(bt_callback);
  byte arr[4];
  for(int i = 0; i < x.size(); i++) {
    float_to_bytes(arr, x[i]);
    SerialBT.write(arr, 4);
    float_to_bytes(arr, y[i]);
    SerialBT.write(arr, 4);
  }
  delay(500);


  // digitalWrite(LED_BUILTIN, HIGH);
  // delay(500);
  // digitalWrite(LED_BUILTIN, LOW);
  // delay(500);
}